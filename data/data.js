module.exports = {
	installed: [
		{
			app: "Fiery Command Workstation Package",
			version: "6.6.0.71",
			description_url: "http://product-redirect.efi.com/cws",
			description_html: `<ul>
                    <li>Manage print productions with CWS</li>
                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </li>
                    <li>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using <i>'Content here, content here'</i>, making it look like readable English.</li>
                </ul>`,
			icon: "/FHM.png",
		},
	],
	explore: [
		{
			app: "Free Form Create",
			version: "1.1.103",
			description_url:
				"http://product-redirect.efi.com/freeformcreate/overview",
			description_html: `<p>
                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.
                </p>`,
			icon: "/spotpro.png",
		},
		{
			app: "Fiery Color Profile Suite",
			version: "5.3.2.17",
			description_html: `<p>
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                </p>
                <ul>
                <li>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. <a href="https://product-redirect.efi.com/fiery/cps/whyupgrade">Find out why</a></li>
                </ul>
                `,
			icon: "/tick.png",
		},
	],
	keys: [
		"HKLM\\SOFTWARE\\Electronics For Imaging\\Fiery Command WorkStation",
		"HKLM\\SOFTWARE\\Electronics For Imaging\\Fiery Command WorkStationsss",
	],
};
