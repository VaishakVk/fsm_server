const express = require("express");
const mockData = require("./data/data");
const constants = require("./constants");
const app = express();
const axios = require("axios");
const regedit = require("regedit");
const fs = require("fs");
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Credentials", "true");
	res.header(
		"Access-Control-Allow-Methods",
		"GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS"
	);
	res.header("Access-Control-Expose-Headers", "Content-Length");
	res.header(
		"Access-Control-Allow-Headers",
		"Accept, Authorization, Content-Type, X-Requested-With, Range"
	);
	if (req.method === "OPTIONS") {
		return res.send(200);
	} else {
		return next();
	}
});

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

app.get("/fsm", (req, res) => {
	const data = mockData;
	res.status(200).send({ data });
});

app.get("/keys", (req, res) => {
	const { platform, locale } = req.query;
	console.log(platform, locale);
	const data = mockData.keys;
	res.status(200).send({ data });
});

app.get("/", async (req, res) => {
	const { c } = req.query;
	console.log(JSON.parse(c));
	const response = await axios.get("http://localhost:8000/fsm");
	res.render("main", {
		data: response.data.data,
	});
});

app.listen("8000", () => {
	console.log("Listening to requests");
});
